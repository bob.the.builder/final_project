class Product < ApplicationRecord
    belongs_to :admin
    has_many :orderlines
    
    has_many :product_tags
    has_many :tags, through: :product_tags
    
    
end
