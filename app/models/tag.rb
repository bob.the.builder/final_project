class Tag < Application_Record
    has_many :product_tags
    has_many :products, through: :product_tags
end
