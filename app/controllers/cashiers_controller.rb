class CashiersController < ApplicationController
    def index
        @cashier = Cashier.all
    end
    
    def new
        @cashier = Cashier.new
    end
    
    def create
        @cashier = Cashier.new(cashier_params)
        @cashier.admin = current_admin
        @cashier.status = true
        @cashier.save
        redirect_to @cashier
    end
    
    def deactivate
        @cashier = Cashier.find(params[:id])
        if @cashier.status
            @cashier.status = false
        else
            @cashier.status = true
        end
        @cashier.status = false
        @cashier.save
        redirect_to cashiers_path
    end
    
    def activate
        @cashier = Cashier.find(params[:id])

        @cashier.status = true
         @cashier.save
        redirect_to cashiers_path
    end
    
    def edit
        @cashier =Cashier.find(params['id'])
        if @cashier.admin != current_admin
            redirect_to cashiers_path
        end
    end
    
    def update 
        @cashier = Cashier.find(params['id'])
        @cashier.update(cashier_params)
        @cashier.save
        redirect_to cashiers_path
    end
    
    def delete
        @cashier = Cashier.find(params[:id])
    end
    
    def destroy
       @cashier = Cashier.find(params['id'])
       @cashier.destroy
       redirect_to cashiers_path
    end
    
    def show
        @cashier = Cashier.find(params['id'])
    end
    
    private
    def cashier_params
        params.require(:cashier).permit!
    end
end
