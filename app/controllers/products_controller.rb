class ProductsController < ApplicationController
    
    def index
        @product = Product.all
    end

    
    def activate
        @product = Product.find(params[:id])
        @product.status = false
        @product.save
        redirect_to products_path
    end
    
    def deactivate
        @product = Product.find(params[:id])
        if @product.status
            @product.status = false
        else
            @product.status = true
        end
        @product.save
        redirect_to products_path
    end
    
    def new
        @product = Product.new
    end
    
    def create
        @product = Product.new(prod_params)
        @product.admin = current_admin
        @product.save
        redirect_to @product
    end
    
    def edit
        @product = Product.find(params['id'])
        if @product.admin != current_admin
            redirect_to products_path
        end
    end
    
    def update
        @product = Product.find(params['id'])
        @product.update(prod_params)
        @product.save
        redirect_to products_path
    end
    
    def delete
        @product = Product.find(params[:id])
    end
    def destroy
        @product = Product.find(params['id'])
        @product.destroy
        redirect_to products_path
    end
    
    def show
        @product = Product.find(params['id'])
    end
    private
    def prod_params
        params.require(:product).permit(:name, :price, :status)
    end
end
