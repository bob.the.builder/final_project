class AddStatusToCashier < ActiveRecord::Migration[5.1]
  def change
    add_column :cashiers, :status, :boolean
  end
end
