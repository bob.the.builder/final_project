class AddPriceToCashier < ActiveRecord::Migration[5.1]
  def change
    add_column :cashiers, :price, :double
  end
end
