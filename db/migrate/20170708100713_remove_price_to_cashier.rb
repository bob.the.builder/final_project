class RemovePriceToCashier < ActiveRecord::Migration[5.1]
  def change
    remove_column :cashiers, :price, :float
  end
end
