Rails.application.routes.draw do
  devise_for :cashiers, only: [:sessions]
  devise_for :admins, only: [:sessions]
  
  resources :products
  post 'products/:id/', to: 'products#deactivate', as: :deactivate_product
  post 'products/:id/', to: 'products#activate', as: :activate_product
    authenticate :admin do
      resources :cashiers, only: [:new, :create, :edit, :update, :destroy, :delete]
      post 'cashiers/:id/', to: 'cashiers#deactivate', as: :deactivate_cashier
      post 'cashiers/:id/', to: 'cashiers#activate', as: :activate_cashier
    end
    authenticate :cashier do
      resources :products, only: [:index]
    end
    
    resources :cashiers, only: [:index, :show]
    resources :public
    resources :orders do
      collection do
        post 'summary'
      end
    end

    post 'about', to: "public#about", as: "about"
    #post 'order_summary', to: "orders#order_summary", as: post
    root to: "public#home"
    
end
